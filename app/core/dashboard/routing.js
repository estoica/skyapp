(function () {
    'use strict';

    dashboard.config(['$stateProvider', function ($stateProvider) {
        $stateProvider.state({
            name        : 'dashboard',
            url         : '/dashboard',
            controller  : 'dashboardController',
            controllerAs: 'vm',
            templateUrl : 'core/dashboard/views/dashboard.html'
        });
    }]);
})();
