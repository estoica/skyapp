(function () {
    'use strict';

    dashboard.constant('DASHBOARD_CONFIG', {
        API_BASE_URL: 'https://opensky-network.org/api/'
    });
})();
