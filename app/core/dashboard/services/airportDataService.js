(function () {
    'use strict';

    dashboard.service('airportDataService', AirportDataService);

    AirportDataService.$inject = ['DASHBOARD_CONFIG', '$http'];

    function AirportDataService(DASHBOARD_CONFIG, $http) {
        return {
            getDetailsForAirport: getDetailsForAirport
        };

        /**
         * Return arrivals / departures for an airport
         * @param airport Airport Object
         * @param timeOffset Time offset in minutes
         * @returns {Promise}
         */
        function getDetailsForAirport(airport, timeOffset) {
            // Using stub file because the API does not permit cross-origin requests
            // Possible solutions - talk with the API devs to include the required headers or use a local proxy
            return $http.get('/app/stub.json').then(function (res) {
                return res.data;
            });
        }
    }
})();
