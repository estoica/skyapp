(function () {
    'use strict';

    dashboard.controller('detailsModalController', DetailsModalController);

    DetailsModalController.$inject = ['airport', 'airportDataService'];

    function DetailsModalController(airport, airportDataService) {
        var vm            = this;
        var VERTICAL_RATE = 11;

        vm.airport    = airport;
        vm.timeOffset = 5;
        vm.arrivals   = [];
        vm.departures = [];

        vm.update = function () {
            return airportDataService.getDetailsForAirport(airport, vm.timeOffset).then(
                function (data) {
                    // clear data
                    vm.arrivals   = [];
                    vm.departures = [];

                    for (var i = 0; i < data.states.length; i++) {
                        if (data.states[i][VERTICAL_RATE] >= 0) {
                            vm.departures.push(data.states[i]);
                        } else {
                            vm.arrivals.push(data.states[i]);
                        }
                    }
                },
                function () {
                    Notification.error('There was a problem retrieving data from API');
                }
            );
        };

        // force first update
        vm.update();
    }
})();
