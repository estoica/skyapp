(function () {
    'use strict';

    dashboard.controller('dashboardController', DashboardController);

    DashboardController.$inject = ['Notification', '$state', 'authService', '$uibModal'];

    function DashboardController(Notification, $state, authService, $uibModal) {
        var vm         = this;
        vm.currentUser = null;

        if (!authService.isAuthenticated()) {
            Notification.error('You must be logged in to view this page');
            $state.go('login');
        } else {
            vm.currentUser = authService.getCurrentUser();
        }

        vm.airports = [
            {name: "Hartsfield–Jackson Atlanta International Airport", country: "USA"},
            {name: "LaGuardia Airport", country: "USA"},
            {name: "Amsterdam Airport Schiphol", country: "Netherlands"},
            {name: "Heathrow Airport", country: "UK"},
            {name: "Beijing Capital International Airport", country: "China"},
            {name: "Tokio International Airport", country: "Japan"},
            {name: "Los Angeles International Airport", country: "USA"},
            {name: "Paris Charles de Gaulle Airport", country: "France"},
            {name: "Dallas Fort Worth International Airport", country: "USA"}
        ];

        vm.logOut = function () {
            return authService.logOut().then(function () {
                return $state.go('login');
            });
        };

        vm.openModal = function (airportId) {
            return $uibModal.open({
                controller    : 'detailsModalController',
                controllerAs  : 'vm',
                windowTopClass: 'details-modal',
                templateUrl   : 'core/dashboard/views/detailsModal.html',
                backdrop      : 'true',
                size          : 'lg',
                resolve       : {
                    airport: vm.airports[airportId]
                }
            });
        };
    }
})();
