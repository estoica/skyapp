(function () {
    auth.controller('loginController', LoginController);

    LoginController.$inject = ['Notification', '$state', 'authService'];

    function LoginController(Notification, $state, authService) {
        var vm = this;

        // redirect the user to dashboard if it's logged in
        if (authService.isAuthenticated()) {
            return $state.go('dashboard');
        }

        vm.form = {
            username: undefined,
            password: undefined
        };

        vm.authenticate = function () {
            return authService.authenticate(vm.form.username, vm.form.password).then(
                function (user) {
                    Notification.success('Welcome ' + user.username + '!');
                    return $state.go('dashboard');
                },
                function () {
                    Notification.error('Invalid credentials');
                }
            );
        };
    }
})();
