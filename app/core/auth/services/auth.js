(function () {
    'use strict';

    auth.service('authService', AuthService);

    AuthService.$inject = ['$q'];

    function AuthService($q) {
        var currentUser = null;

        return {
            authenticate   : authenticate,
            isAuthenticated: isAuthenticated,
            getCurrentUser : getCurrentUser,
            logOut         : logOut
        };

        /**
         * @param username
         * @param password
         * @returns {Promise}
         */
        function authenticate(username, password) {
            var deferred = $q.defer();

            // TODO - use a proper user validation through a backend service
            if (username === 'demo' && password === 'demo') {
                currentUser = {
                    username: username,
                    password: password
                };
                deferred.resolve(currentUser);
            } else {
                deferred.reject();
            }

            return deferred.promise;
        }

        /**
         * @returns {boolean}
         */
        function isAuthenticated() {
            return currentUser !== null;
        }

        /**
         * @returns {Object|null}
         */
        function getCurrentUser() {
            return currentUser;
        }

        /**
         * @returns {Promise}
         */
        function logOut() {
            currentUser = null;
            return $q.when();
        }
    }
})();
