(function () {
    'use strict';

    auth.config(['$stateProvider', function ($stateProvider) {
        $stateProvider.state({
            name        : 'login',
            url         : '/login',
            controller  : 'loginController',
            controllerAs: 'vm',
            templateUrl : 'core/auth/views/login.html'
        });
    }]);
})();
