(function () {
    'use strict';

    var main = angular.module('main', ['ui.router', 'ui-notification', 'ui.bootstrap', 'auth', 'dashboard']);

    main.config(['$qProvider', '$locationProvider', '$urlRouterProvider',
        function ($qProvider, $locationProvider, $urlRouterProvider) {
            $qProvider.errorOnUnhandledRejections(false);
            $locationProvider.html5Mode(true);

            // fallback on login page
            $urlRouterProvider.otherwise('/login');
        }]);
})();
