# skyapp
Demo application that provide flights data based on an external API

## Prerequisites
* NodeJS >= 4.7.2
* NPM >= 2.15.11
* Bower >= 1.8.0
* GulpJS >= 3.9.1

## Installation
* Clone this repository using `git clone`
* Install NPM dependencies using `npm install`
* Install Bower dependencies using `bower install`

## Considerations
* The API provided does not permit cross-origin requests - the solution was either to mock-up the result locally (just for the demo) or to develop a proxy service that returns the right cross-origin headers (long term solution).
* The API provided (opensky) does not permit querying for a given airport or city and does not contain details like departures / arrivals. 
* To solve those problems I made a stub.json file in the root of the app which simulates the API behaviour even though it is just a static resource which does not alter by querying with different arguments.
* In the airport details modal I partitioned states by the following rule: if the vertical rate is positive then it's a departure, otherwise it's an arrival

## Other notable features
* I used a notification module to prompt notices and errors.
* When an unauthorized user lands on the dashboard page the app will automatically redirect him to the login page.
* I used `angular-bootstrap` module which allowed me to manage the modals programmatically

## Config
* Change parameters from config.js files located in the modules root folders

## Run a local webserver
* `gulp webserver`